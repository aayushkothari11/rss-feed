from django.db import models
from datetime import date
# Create your models here.

class News_DB(models.Model):
    news = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    keyword = models.CharField(max_length=200, blank=True, null=True)
    time = models.DateField(default=date.today, blank=True)
