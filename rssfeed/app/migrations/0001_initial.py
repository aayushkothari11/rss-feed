# Generated by Django 3.0.8 on 2020-07-27 15:15

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News_DB',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('news', models.CharField(blank=True, max_length=200, null=True)),
                ('description', models.CharField(blank=True, max_length=200, null=True)),
                ('keyword', models.CharField(blank=True, max_length=200, null=True)),
                ('time', models.DateField(blank=True, default=datetime.date.today)),
            ],
        ),
    ]
