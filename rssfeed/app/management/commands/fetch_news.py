from django.core.management.base import BaseCommand
from app.models import News_DB
import requests
from bs4 import BeautifulSoup
from nltk.tokenize import MWETokenizer
from nltk import sent_tokenize, word_tokenize
import string
import datetime

class Command(BaseCommand):

    def handle(self, *args, **kwargs):

        objs = News_DB.objects.all()
        unique_keywords = set(obj.keyword for obj in objs)

        url = 'https://economictimes.indiatimes.com/rssfeedsdefault.cms'
        response = requests.get(url)
        soup = BeautifulSoup(response.content, features = "lxml")
        items = soup.findAll('item')
        news_items = []
        for item in items:
            news_item = {}
            news_item['title'] = item.title.text
            news_item['description'] = item.description.text
            temp = item.pubdate.text
            news_item['pubdate'] = temp.split('T')[0]
            news_items.append(news_item)

        for word in unique_keywords:

            keywords = {word}

            mwe = MWETokenizer([k.lower().split() for k in keywords], separator='_')
            puncts = list(string.punctuation)

            key_word_items = []

            for news_item in news_items:
                paragraph = news_item['title']
                cleaned_paragraph = ''.join([ch if ch not in puncts else '' for ch in paragraph.lower()])
                tokenized_paragraph = [token for token in mwe.tokenize(word_tokenize(cleaned_paragraph)) if token.replace('_', ' ') in keywords]
                if len(tokenized_paragraph)>0:
                    if not News_DB.objects.filter(news=news_item['title'], keyword=word):
                        obj = News_DB.objects.create(news=news_item['title'], description=news_item["description"], keyword=word, time=news_item["pubdate"])
                        obj.save()

                        today = datetime.date.today()
                        delta = today - obj.time
                        if delta.days > 2:
                            obj.delete()
