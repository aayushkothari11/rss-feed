from django.core.management.base import BaseCommand
import datetime
from app.models import News_DB

class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        today = datetime.date.today()
        objs = News_DB.objects.all()
        for obj in objs:
            delta = today - obj.time
            if delta.days > 2:
                obj.delete()
